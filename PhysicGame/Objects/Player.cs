﻿using System;
using System.Collections.Generic;
using GameLibrary.Movement;
using GameLibrary.Graphic;
using GameLibrary.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PhysicGame.Objects.Interfaces;

namespace PhysicGame.Objects
{
    class Player : Creature, IMonster
    {
        private PlayerAnimator _anim;
        private readonly PlayerWorker _playerControll;

        public int Width { get; set; } = 32;
        public int Height { get; set; } = 48;
        public CollisionMap CollisionMap { get; set; }

        public Player(Game game, CollisionMap collisionMap) : base(game)
        {
            _playerControll = new PlayerWorker(game);
            _playerControll.Keyboard.PressedEscape += PressedEscape;
            Velocity = 200;
            CollisionMap = collisionMap;
        }

        private void PressedEscape(object sender, EventArgs e)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
            HpChanged += OnHpChanged;
            _anim.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            Texture = Game.Content.Load<Texture2D>("Sprites\\player");
            _anim = new PlayerAnimator(Game, Texture)
            {
                Position = Position,
                HeightSingleSprite = Height,
                WidthSingleSprite = Width
            };
        }

        public void Update(GameTime gameTime, Camera2D _camera)
        {
            base.Update(gameTime);
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _playerControll.Update(gameTime);

            var states = _playerControll.State;

            if (_playerControll.IsMoving)
            {
                _anim.AnimationTime = 135;
            }
            else if (!_playerControll.IsMoving)
            {
                _anim.AnimationTime = 385;
            }

            MovementState state;
            if (states.Contains(MovementState.Up))
            {
                state = MovementState.Up;
                Position.Y -= Velocity * deltaTime;
                Collision(state, _camera, deltaTime);
                _camera.Position -= new Vector2(0, Velocity) * deltaTime;

                _playerControll.State.Remove(state);
            }
            if (states.Contains(MovementState.Righ))
            {
                state = MovementState.Righ;
                Position.X += Velocity * deltaTime;
                Collision(state, _camera, deltaTime);
                _camera.Position += new Vector2(Velocity, 0) * deltaTime;

                _playerControll.State.Remove(state);
            }
            if (states.Contains(MovementState.Down))
            {
                state = MovementState.Down;
                Position.Y += Velocity * deltaTime;
                Collision(state, _camera, deltaTime);
                _camera.Position += new Vector2(0, Velocity) * deltaTime;

                _playerControll.State.Remove(state);
            }
            if (states.Contains(MovementState.Left))
            {
                state = MovementState.Left;
                Position.X -= Velocity * deltaTime;
                Collision(state, _camera, deltaTime);
                _camera.Position -= new Vector2(Velocity, 0) * deltaTime;

                _playerControll.State.Remove(state);
            }
            _anim.Update(gameTime);




            _playerControll.State = states;
        }

        private bool Collision(MovementState direction, Camera2D _camera, float deltaTime)
        {
            foreach (var tile in CollisionMap.CollisionTiles)
            {
                if (tile.Bounds.Intersects(new Rectangle((int)Position.X, (int)Position.Y, Width, Height)))
                {
                    var collisionRectangle = Rectangle.Intersect(
                          new Rectangle(tile.Bounds.X, tile.Bounds.Y, tile.Bounds.Width, tile.Bounds.Height)
                        , new Rectangle((int)Position.X, (int)Position.Y, Width, Height));
                    switch (direction)
                    {
                        case MovementState.Down:
                            Position.Y -= collisionRectangle.Height;
                            _camera.Position -= new Vector2(0, collisionRectangle.Height);
                            break;
                        case MovementState.Up:
                            Position.Y += collisionRectangle.Height;
                            _camera.Position += new Vector2(0, collisionRectangle.Height);
                            break;
                        case MovementState.Left:
                            Position.X += collisionRectangle.Width;
                            _camera.Position += new Vector2(collisionRectangle.Width, 0);
                            break;
                        case MovementState.Righ:
                            Position.X -= collisionRectangle.Width;
                            _camera.Position -= new Vector2(collisionRectangle.Width, 0);
                            break;
                    }
                    return true;
                }
            }
            return false;
        }

        public void Draw(SpriteBatch spriteBatch, Camera2D _camera)
        {
            spriteBatch.Begin();

            spriteBatch.End();
            _anim.Draw(spriteBatch, _playerControll.ControlState, _playerControll.IsMoving, _camera);

            _playerControll.IsMoving = false;
        }

        public void OnHpChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private class PlayerWorker : GameComponent, IControllable
        {
            public KeyboardControl Keyboard { get; }
            public MovementState ControlState { get; private set; }
            public List<MovementState> State { get; set; }
            public bool IsMoving { get; set; }

            public PlayerWorker(Game game) : base(game)
            {
                State = new List<MovementState>();
                Keyboard = new KeyboardControl(game);

                Keyboard.PressedUp += PressedUp;
                Keyboard.PressedRight += PressedRight;
                Keyboard.PressedDown += PressedDown;
                Keyboard.PressedLeft += PressedLeft;
            }

            public override void Update(GameTime gameTime)
            {
                base.Update(gameTime);
                Keyboard.Update(gameTime);
            }

            public void PressedLeft(object sender, EventArgs e)
            {
                ControlState = MovementState.Left;
                State.Add(MovementState.Left);
                IsMoving = true;
            }

            public void PressedDown(object sender, EventArgs e)
            {
                ControlState = MovementState.Down;
                State.Add(MovementState.Down);
                IsMoving = true;
            }

            public void PressedRight(object sender, EventArgs e)
            {
                ControlState = MovementState.Righ;
                State.Add(MovementState.Righ);
                IsMoving = true;
            }

            public void PressedUp(object sender, EventArgs e)
            {
                ControlState = MovementState.Up;
                State.Add(MovementState.Up);
                IsMoving = true;
            }
        }
    }
}
