﻿using System;
using GameLibrary.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using I = PhysicGame.Objects.Interfaces;

namespace PhysicGame.Objects
{
    internal class Creature : DrawableGameComponent, I.IDrawable
    {
        public event EventHandler HpChanged;
        public event EventHandler<PositionEventArgs> PositionChanged;


        public int Velocity { get; set; }
        public double MaxHp { get; set; }

        private Texture2D _texture;
        public Texture2D Texture
        {
            get { return _texture ?? (_texture = Game.Content.Load<Texture2D>(@"Textures\Default")); }
            set { _texture = value; }
        }

        private Position _position;
        public Position Position
        {
            get { return _position; }
            set
            {
                _position = value;
                PositionEventArgs args = new PositionEventArgs(_position);
                OnPositionChanged(args);
            }
        }


        private double _hp;
        public double Hp
        {
            get { return _hp; }
            set
            {
                _hp = value; 
                OnHpChanged(EventArgs.Empty);
            }
        }


        public Creature(Game game) : base(game)
        {
            _position = new Position(100,100);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        protected virtual void OnHpChanged(EventArgs e)
        {
            EventHandler eh = HpChanged;
            eh?.Invoke(this, e);
        }

        public virtual void OnPositionChanged(PositionEventArgs e)
        {
            EventHandler<PositionEventArgs> eh = PositionChanged;
            eh?.Invoke(this, e);
        }
    }
}
