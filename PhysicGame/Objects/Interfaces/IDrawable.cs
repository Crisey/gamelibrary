﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicGame.Objects.Interfaces
{
    public interface IDrawable
    {
        Texture2D Texture { get; set; }
        bool Visible { get; set; }

        void Draw(GameTime gameTime);
    }
}