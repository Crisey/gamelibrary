﻿using System;
using GameLibrary.Map;

namespace PhysicGame.Objects.Interfaces
{
    interface IMonster
    {
        double Hp { get; set; }
        double MaxHp { get; set; }
        int Velocity { get; set; }
        Position Position { get; set; }

        event EventHandler<PositionEventArgs> PositionChanged;
        event EventHandler HpChanged;

        void OnPositionChanged(PositionEventArgs e);
        void OnHpChanged(object sender, EventArgs e);
    }
}