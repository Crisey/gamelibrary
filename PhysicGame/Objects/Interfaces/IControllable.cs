﻿using System;
using GameLibrary.Movement;

namespace PhysicGame.Objects.Interfaces
{
    public interface IControllable
    {
        void PressedUp(object sender, EventArgs e);
        void PressedRight(object sender, EventArgs e);
        void PressedDown(object sender, EventArgs e);
        void PressedLeft(object sender, EventArgs e);

        bool IsMoving { get; set; }
        MovementState ControlState { get; }
        KeyboardControl Keyboard { get; }
    }
}