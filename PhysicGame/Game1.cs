﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PhysicGame.Objects;
using GameLibrary.UI.Controls;
using GameLibrary.Graphic;
using GameLibrary.Graphic.Effects;
using GameLibrary.UI.Core;
using GameLibrary.Utils;
using GameLibrary.Map;

namespace PhysicGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicManager graphManager;
        SpriteBatch spriteBatch;
        private readonly Button _button;
        private readonly Textbox _textbox;
        private RadioArea _area;
        private Checkbox checkbox;
        private Checkbox checkbox1;
        private Radiobox radiobox;
        private Radiobox radio1;
        private Radiobox radio2;
        private Form form;
        private Camera2D _camera;
        private FramerateCounter fpsCounter;
        private double elapsedTime;
        private Player _player;
        private World _world;
        CameraEffects effects;


        public Game1()
        {
            var graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphManager = new GraphicManager();
            graphManager.DebugMode = true;

            IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferMultiSampling = true;
          
            //graphics.IsFullScreen = true;

            _button = new Button(this);
            _textbox = new Textbox(this);
            _button.AutoPosition = false;
            radiobox = new Radiobox(this);
            radio1 = new Radiobox(this);
            radio2 = new Radiobox(this);
            checkbox = new Checkbox(this);
            checkbox1 = new Checkbox(this);
            _area = new RadioArea();
            form = new Form(this);

            fpsCounter = new FramerateCounter(this);
                     /*  graphics.SynchronizeWithVerticalRetrace = false;
                        this.IsFixedTimeStep = false;*/
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            _world = new World(this,
            @"..\..\..\..\World\Map01.xml")
            { TileSize = 64 };
            _world.Initialize();

            _player = new Player(this, _world.Map.CollisionMap);
            _player.Initialize();
            _camera = new Camera2D(GraphicsDevice.Viewport);
            _camera.Position = new Vector2(-GraphicsDevice.Viewport.Bounds.Width / 2 + _player.Position.X + _player.Width / 2,
                                           -GraphicsDevice.Viewport.Bounds.Height / 2 + _player.Height / 2 + _player.Position.Y);
            radiobox.Bounds = new Rectangle(350, 555, 32, 32);
            _textbox.Bounds = new Rectangle(350, 475, 200, 24);
            radio1.Bounds = new Rectangle(350, 600, 32, 32);
            radio2.Bounds = new Rectangle(350, 655, 32, 32);
            _button.Bounds = new Rectangle(350, 515, 0, 0);

            radiobox.IsChecked = true;
            _button.AutoPosition = true;

            _button.Text.Value = "Test button";
            radiobox.Text.Value = "Test Radiobox Checked";
            _textbox.Text.Value = "TEST TEXTBOX ` ~ 1 ! 2 @ 3 # 4 $ 5 % 6 ^ 7 & 8 * 9 ( 0 )       ";
            _textbox.Margin = 10;
            _textbox.Text.Color = Color.BlueViolet;
            radio1.Text.Value = "Test Radiobox Unchecked";
            radio2.Text.Value = "Test 1";
            checkbox.Text.Value = "e.g. checkbox";
            checkbox1.Text.Value = "e.g. checkbox";

            _area.AddToList(radiobox);
            _area.AddToList(radio1);
            _area.AddToList(radio2);

            checkbox.Bounds = new Rectangle(675, 555, 32, 32);
            checkbox1.Bounds = new Rectangle(10, 10, 32, 32);


            _button.MouseDown += _button_Click;
            form.Bounds = new Rectangle(50, 50, 400, 425);
            form.AddControl(checkbox1);

            effects = new CameraEffects(this, _camera);
        }

        private void _button_Click(object sender, EventArgs e)
        {
            form.Visible = true;
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            _button.LoadContent();
            _textbox.LoadContent();
            radiobox.LoadContent();
            radio1.LoadContent();
            radio2.LoadContent();
            checkbox.LoadContent();
            checkbox1.LoadContent();
            form.LoadContent();


            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            Content.Unload();
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
            _player.Update(gameTime, _camera);
            _button.Update(gameTime);
            _textbox.Update(gameTime);
            radiobox.Update(gameTime);
            radio1.Update(gameTime);
            radio2.Update(gameTime);
            checkbox.Update(gameTime);
            form.Update(gameTime);
            fpsCounter.Update(gameTime);
            effects.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            _world.Draw(spriteBatch, _camera, true, false);
            _player.Draw(spriteBatch, _camera);

            _button.Draw(spriteBatch);
            _textbox.Draw(spriteBatch);
            radiobox.Draw(spriteBatch);
            radio1.Draw(spriteBatch);
            radio2.Draw(spriteBatch);
            checkbox.Draw(spriteBatch);
            //checkbox1.Draw(spriteBatch);
            form.Draw(spriteBatch);
            fpsCounter.Draw(spriteBatch);
        }
    }
}