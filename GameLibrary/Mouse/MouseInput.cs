﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameLibrary.Mouse
{
    class MouseInput : GameComponent
    {
        public event EventHandler MousePositionChanged;
        public event EventHandler MouseLeftClick;
        public event EventHandler MouseRightClick;
        public event EventHandler MouseDown;
        public event EventHandler MouseUp;
       
        private MouseState _oldState;

        private Point _mousePosition;
        public Point MousePosition
        {
            get { return _mousePosition; }
            set
            {
                _mousePosition = value;
                OnMousePositionChanged(EventArgs.Empty);
            }
        }


        public MouseInput(Game game) : base(game)
        {

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            MouseState newState = Microsoft.Xna.Framework.Input.Mouse.GetState();

            if (newState.Position != _oldState.Position)
            {
                MousePosition = newState.Position;
            }
            if (newState.LeftButton == ButtonState.Pressed && _oldState.LeftButton == ButtonState.Released)
            {
                OnMouseLeftClick(EventArgs.Empty);
            }
            if (newState.RightButton == ButtonState.Pressed && _oldState.RightButton == ButtonState.Released)
            {
                OnMouseRightClick(EventArgs.Empty);
            }
            if (newState.LeftButton == ButtonState.Pressed)
            {
                OnMouseDown(EventArgs.Empty);
            }
            if (newState.LeftButton == ButtonState.Released && _oldState.LeftButton == ButtonState.Pressed)
            {
                OnMouseUp(EventArgs.Empty);
            }

            _oldState = newState;
        }

        protected virtual void OnMouseDown(EventArgs e)
        {
            EventHandler eh = MouseDown;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseUp(EventArgs e)
        {
            EventHandler eh = MouseUp;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseRightClick(EventArgs e)
        {
            EventHandler eh = MouseRightClick;
            eh?.Invoke(this, e);
        }

        protected virtual void OnMouseLeftClick(EventArgs e)
        {
            EventHandler eh = MouseLeftClick;
            eh?.Invoke(this, e);
        }
        
        protected virtual void OnMousePositionChanged(EventArgs e)
        {
            EventHandler eh = MousePositionChanged;
            eh?.Invoke(this, e);
        }
    }
}
