﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Map
{
    public class World : DrawableGameComponent
    {
        private readonly TileManager.Tile _tile;
        public Map Map { get; set; }
        public string Path { get; set; }
        public int TileSize { get; set; }

        public World(Game game, string path) : base(game)
        {
            Path = path;
            _tile = new TileManager.Tile(Game);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            
        }

        public override void Initialize()
        {
            base.Initialize();
            Map = new Map(Game) {TileSize = TileSize};
            Map.Initialize(Path);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        public void Draw(SpriteBatch spriteBatch, Camera2D _camera, bool debugMode, bool frame = true)
        {
            var viewMatrix = _camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: viewMatrix);
            foreach (var column in Map.TileManager.Rows.SelectMany(row => row.Columns))
            {
                column.Tile.Draw(spriteBatch, debugMode);
                if (!frame) continue;
                _tile.Bounds = column.Tile.Bounds;
                _tile.TitlePath = "Textures/frame";
                _tile.Draw(spriteBatch, debugMode);
            }
           spriteBatch.End();
        }
    }
}
