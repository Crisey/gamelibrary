﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Graphic;

namespace GameLibrary.Map
{
    public class CollisionMap
    {
        public List<TileManager.Tile> CollisionTiles { get; set; }

        public CollisionMap()
        {
            CollisionTiles = new List<TileManager.Tile>();
        }
    }

    public enum CollisionType
    {
        None,
        Block,
    }

    public enum CollisionDirection
    {
        Horizontal,
        Vertical
    }
}
