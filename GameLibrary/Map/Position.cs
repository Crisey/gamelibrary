﻿using System;
using Microsoft.Xna.Framework;

namespace GameLibrary.Map
{
    public class Position
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X,Y);
        }
    }

    public class PositionEventArgs : EventArgs
    {
        public Position Position { get; private set; }

        public PositionEventArgs(Position position)
        {
            Position = position;
        }
    }
}
