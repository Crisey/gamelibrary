﻿using System;
using System.Collections.ObjectModel;
using GameLibrary.Map;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.Graphic
{
    public class TileManager : DrawableGameComponent
    {
        public Collection<Row> Rows { get; set; }

        public TileManager(Game game) : base(game)
        {
            Rows = new Collection<Row>();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
            Game.Content.Unload();
        }

        public class Row
        {
            public Collection<Column> Columns { get; set; }

            public Row()
            {
                Columns = new Collection<Column>();
            }
        }

        public class Column
        {
            public Tile Tile { get; set; }
        }

        public class Tile : DrawableGameComponent
        {
            private FontsUtil _font;
            public Texture2D Texture { get; set; }
            public Rectangle Bounds { get; set; }
            public CollisionType CollisionType { get; set; } = CollisionType.None;

            private string _titlePath;
            public string TitlePath
            {
                get { return _titlePath; }
                set
                {
                    _titlePath = value;
                    try
                    {
                        Texture = Game.Content.Load<Texture2D>(_titlePath);
                    }
                    catch (ContentLoadException ex)
                    {
                        Console.WriteLine(ex.Message);
                        Texture = Game.Content.Load<Texture2D>("Textures/Default");
                    }
                }
            }

            public Tile(Game game) : base(game)
            {
                TitlePath = "Textures/Default";
                _font = new FontsUtil(game);
            }

            public Tile(Game game, Rectangle bounds, string titlePath = "Textures/Default") : base(game)
            {
                TitlePath = titlePath;
                Bounds = bounds;
                _font = new FontsUtil(game);
            }

            public void Draw(SpriteBatch spriteBatch, bool debugMode)
            {
                //spriteBatch.Begin();
                if (debugMode && CollisionType == CollisionType.Block)
                {
                    spriteBatch.Draw(Texture, Bounds, Color.Red);
                    spriteBatch.DrawString(_font.MediumFont, CollisionType.Block.ToString(), new Vector2(Bounds.X, Bounds.Y), Color.White);
                }
                else
                    spriteBatch.Draw(Texture, Bounds, Color.White);

                //spriteBatch.End();
            }
        }
    }
}
