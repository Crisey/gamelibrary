﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GameLibrary.Utils;
using Microsoft.Xna.Framework;

namespace GameLibrary.Graphic.Effects
{
    public class CameraEffects : GameComponent
    {
        public Camera2D Camera { get; set; }

        private ThreadSafeRandom _rnd;
        private double elapsedTime;

        public CameraEffects(Game game, Camera2D camera) : base(game)
        {
            _rnd = new ThreadSafeRandom();

            Camera = camera;
        }

      /*  public void ShakingCamera(GameTime gameTime, Vector2 actualPos, int x, int y, int delay = 25, int times = 1, int rndXMin = 1, int rndXMax = 1, int rndYMin = 1, int rndYMax = 1)
        {
            int xM = 0;
            int yM = 0;

            Task.Factory.StartNew(() =>
            {
                elapsedTime = 0;
                for (int i = 0; i < times; i++)
                {
                    Camera.Position += new Vector2(x, y);
                    xM += -x;
                    yM += -y;
                }

                Task.Delay(delay).Wait();
                Camera.Position += new Vector2(xM, yM);
            });

        }*/

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
        }
    }
}
