﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameLibrary.Map;
using GameLibrary.Movement;

namespace GameLibrary.Graphic
{
    public class PlayerAnimator : DrawableGameComponent
    {
        private double _timer;
        private int _timerCount;

        public int WidthSingleSprite { get; set; }
        public int HeightSingleSprite { get; set; }
        public Position Position { get; set; }
        public Texture2D Texture { get; set; }
        public double AnimationTime { get; set; }

        public PlayerAnimator(Game game) : base(game)
        {

        }

        public PlayerAnimator(Game game, Texture2D texture) : base(game)
        {
            Texture = texture;
        }

        public override void Update(GameTime gameTime)
        {
            //base.Update(gameTime);
           // var delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //And now I can use delta to apply movement like this:
            base.Update(gameTime);

            _timer += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (!(_timer >= AnimationTime)) return;
            var count = Texture.Width / WidthSingleSprite;
            Console.WriteLine(_timer);
            if (count - 1 > _timerCount)
                _timerCount++;
            else
                _timerCount = 0;
            _timer = 0;
        }

        public void Draw(SpriteBatch spriteBatch, MovementState state, bool isMoving, Camera2D _camera)
        {
            var viewMatrix = _camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: viewMatrix);
            if (state == MovementState.Down && isMoving)
                DrawAnim(0, spriteBatch);
            else if (state == MovementState.Left && isMoving)
                DrawAnim(48, spriteBatch);
            else if (state == MovementState.Righ && isMoving)
                DrawAnim(96, spriteBatch);
            else if (state == MovementState.Up && isMoving)
                DrawAnim(144, spriteBatch);
            else if (!isMoving)
                DrawAnim(192, spriteBatch);
            spriteBatch.End();
        }

        private void DrawAnim(int y, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture,
                   new Rectangle((int)Position.X, (int)Position.Y, WidthSingleSprite, HeightSingleSprite),
                   new Rectangle(_timerCount * WidthSingleSprite, y, WidthSingleSprite, HeightSingleSprite),
                   Color.White);
        }
    }
}
