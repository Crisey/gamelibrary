﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameLibrary.Utils;

namespace GameLibrary.Graphic.Particles
{
    public class Particle : DrawableGameComponent
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public float Angle { get; set; }
        public float AngularVelocity { get; set; }
        public Color Color { get; set; }
        public float Size { get; set; }
        public int TimeLife { get; set; }
        private readonly ThreadSafeRandom _rnd;

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity,
            float angle, float angularVelocity, Color color, float size, int ttl, Game game) : base(game)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            TimeLife = ttl;
            _rnd = new ThreadSafeRandom();
        }

        public void Update(GameTime gameTime, Action particleBehavior)
        {
            TimeLife -= (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            base.Update(gameTime);

            if (particleBehavior != null)
                particleBehavior();
            DefaultParticleBehavior();

            Position += Velocity;
            Angle += AngularVelocity;


        }

        private void DefaultParticleBehavior()
        {
            if (TimeLife <= 215)
            {
                Color *= 0.8825f;
                if (Size > 0.05f)
                {
                    Size -= 0.035f;
                    // Thread.Sleep(100);
                }
            }

            if (Color.A <= 50)
            {
                TimeLife = 0;
            }
            // Console.WriteLine(transparency * 100f);

            if (Velocity.Y >= 2f + _rnd.NextDouble(-10, 10) / 100)
            {
                Velocity = new Vector2(Velocity.X, Velocity.Y - (0.0375f + (float)_rnd.NextDouble(-7, 13) / 1000));
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            var origin = new Vector2(Texture.Width / 2, Texture.Height / 2);


            spriteBatch.Draw(Texture, Position, sourceRectangle, Color,
                Angle, origin, Size, SpriteEffects.None, 0f);
        }
    }
}
