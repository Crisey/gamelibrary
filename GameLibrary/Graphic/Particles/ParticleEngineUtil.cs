﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary.Graphic.Particles
{
    public class ParticleEngineUtil
    {
        public int TimeLifeRnd { get; set; }

        public int SizeMinRnd { get; set; }
        public int SizeMaxRnd { get; set; }

        public int AngleMinRnd { get; set; }
        public int AngleMaxRnd { get; set; }

        public int AngularVelocityMinRnd { get; set; }
        public int AngularVelocityMaxRnd { get; set; }

        public int VelocityXMinRnd { get; set; }
        public int VelocityXMaxRnd { get; set; }

        public int VelocityYMinRnd { get; set; }
        public int VelocityYMaxRnd { get; set; }

        public ParticleEngineUtil()
        {
            
        }

        public ParticleEngineUtil(int timeLifeRnd, int sizeMinRnd, int sizeMaxRnd, int angleMinRnd, int angleMaxRnd, int angularVelocityMinRnd, int angularVelocityMaxRnd, int velocityXMinRnd, int velocityXMaxRnd, int velocityYMinRnd, int velocityYMaxRnd)
        {
            TimeLifeRnd = timeLifeRnd;
            SizeMinRnd = sizeMinRnd;
            SizeMaxRnd = sizeMaxRnd;
            AngleMinRnd = angleMinRnd;
            AngleMaxRnd = angleMaxRnd;
            AngularVelocityMinRnd = angularVelocityMinRnd;
            AngularVelocityMaxRnd = angularVelocityMaxRnd;
            VelocityXMinRnd = velocityXMinRnd;
            VelocityXMaxRnd = velocityXMaxRnd;
            VelocityYMinRnd = velocityYMinRnd;
            VelocityYMaxRnd = velocityYMaxRnd;
        }
    }
}
