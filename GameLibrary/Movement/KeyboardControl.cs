﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameLibrary.Movement
{
    public class KeyboardControl : GameComponent
    {
        public event EventHandler PressedUp;
        public event EventHandler PressedRight;
        public event EventHandler PressedDown;
        public event EventHandler PressedLeft;
        public event EventHandler PressedEscape;

        private KeyboardState _state;

        public KeyboardControl(Game game) : base(game)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            _state = Keyboard.GetState();

            if (HandleInput(Keys.W) || HandleInput(Keys.Up))
            {
                OnPressedUp(EventArgs.Empty);
            }
            if (HandleInput(Keys.D) || HandleInput(Keys.Right))
            {
                OnPressedRight(EventArgs.Empty);
            }
            if (HandleInput(Keys.S) || HandleInput(Keys.Down))
            {
                OnPressedDown(EventArgs.Empty);
            }
            if (HandleInput(Keys.A) || HandleInput(Keys.Left))
            {
                OnPressedLeft(EventArgs.Empty);
            }
            if (HandleInput(Keys.Escape))
            {
                OnPressedEscape(EventArgs.Empty);
            }
        }

        private bool HandleInput(Keys key)
        {
            return (_state.IsKeyDown(key));
        }

        #region Events
        protected virtual void OnPressedUp(EventArgs e)
        {
            EventHandler eh = PressedUp;
            eh?.Invoke(this, e);
        }

        protected virtual void OnPressedRight(EventArgs e)
        {
            EventHandler eh = PressedRight;
            eh?.Invoke(this, e);
        }

        protected virtual void OnPressedDown(EventArgs e)
        {
            EventHandler eh = PressedDown;
            eh?.Invoke(this, e);
        }

        protected virtual void OnPressedLeft(EventArgs e)
        {
            EventHandler eh = PressedLeft;
            eh?.Invoke(this, e);
        }

        protected virtual void OnPressedEscape(EventArgs e)
        {
            EventHandler eh = PressedEscape;
            eh?.Invoke(this, e);
        }
        #endregion Events
    }
}
