﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    class Image : Control
    {
        public Image(Game game) : base(game)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, Text.Value, new Vector2(Bounds.X + Text.Position.X, Bounds.Y + Text.Position.Y), Text.Color);
            spriteBatch.End();

        }
    }
}
