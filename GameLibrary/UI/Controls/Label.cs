﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.Map;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Label : DrawableGameComponent
    {
        private readonly FontsUtil _fonts;
        public Text Text { get; set; }

        public Label(Game game) : base(game)
        {
            _fonts = new FontsUtil(game);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(_fonts.MediumFont, Text.Value, Text.Position, Text.Color);
            spriteBatch.End();
        }
    }
}
