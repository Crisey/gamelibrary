﻿using System;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls.Styles
{
    public class CheckboxStyle : Control
    {
        #region Props

        private bool _isChecked = false;
        private Texture2D _checkedTexture;
        private Texture2D _uncheckedTexture;

        public int Margin { get; set; } = 5;


        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                Texture = value ? CheckedTexture : UncheckedTexture;
                _isChecked = value;
                OnCheckedChanged(EventArgs.Empty);
            }
        }

        protected Texture2D CheckedTexture
        {
            get
            {
                return _checkedTexture ?? Game.Content.Load<Texture2D>("Textures/Default");
            }
            set { _checkedTexture = value; }
        }

        protected Texture2D UncheckedTexture
        {
            get
            {
                return _uncheckedTexture ?? Game.Content.Load<Texture2D>("Textures/Default");
            }
            set { _uncheckedTexture = value; }
        }

        #endregion Props    

        public CheckboxStyle(Game game) : base(game)
        {
            Click += OnClick;
        }

        public void LoadContent()
        {
            base.LoadContent();
            IsChecked = false;
            BoundsChanged += (sender, args) =>
            {
                Text.Position = new Vector2(Bounds.X + Bounds.Width + Margin,
                                           Bounds.Y + Bounds.Height / 2 - (Font.MediumFont.MeasureString(Text.Value).Y / 2) + 1);
            };
        }

        public virtual void OnClick(object sender, EventArgs e)
        {
                IsChecked = !IsChecked;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, Text.Value, Text.Position, Text.Color);
            spriteBatch.End();
        }

        #region events
        public event EventHandler CheckedChanged;
        protected virtual void OnCheckedChanged(EventArgs e)
        {
            EventHandler eh = CheckedChanged;
            if (eh != null)
            {
                eh(this, e);
            }
        }
        #endregion events
    }
}
