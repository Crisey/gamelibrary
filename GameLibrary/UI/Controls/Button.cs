﻿using System;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Button : Control
    {
        public Point Margin { get; set; } = new Point(24, 6);

        private bool _autoPosition = true;

        public bool AutoPosition
        {
            get { return _autoPosition; }
            set
            {
                _autoPosition = value;
                if (value)
                {
                    Bounds = new Rectangle(Bounds.X, Bounds.Y,
                        (int)Font.MediumFont.MeasureString(Text.Value).Length() + Margin.X,
                        (int)Font.MediumFont.MeasureString(Text.Value).Y + Margin.Y);
                }
            }
        }

        public override Rectangle Bounds
        {
            get { return _bounds; }
            set
            {
                _bounds = value;
                if (value.Width <= 0 || value.Height <= 0)
                    AutoPosition = true;
                OnBoundsChanged(EventArgs.Empty);
            }
        }

        public Button(Game game) : base(game)
        {
            Text.ValueChanged += (sender, args) =>
            {
                if (AutoPosition)
                    AutoPosition = true;

            };
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            spriteBatch.Begin();
            spriteBatch.DrawString(Font.MediumFont, Text.Value, Text.Position, Text.Color);
            spriteBatch.End();
        }

        public void LoadContent()
        {
            base.LoadContent();

            Texture = Game.Content.Load<Texture2D>("Textures/UI/Button");


            BoundsChanged += (sender, args) =>
            {
                Text.Position = new Vector2(x: Bounds.X + (Bounds.Width / 2) - (Font.MediumFont.MeasureString(Text.Value).X / 2),
                                           y: Bounds.Y + (Bounds.Height / 2) - (Font.MediumFont.MeasureString(Text.Value).Y / 2) + 1);
            };
        }

        #region Events

        #endregion Events
    }
}
