﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.UI.Controls;

namespace GameLibrary.UI.Core
{
    public class RadioArea
    {
        private readonly List<Radiobox> _radioButtons;

        public RadioArea()
        {
            _radioButtons = new List<Radiobox>();
        }

        public void AddToList(Radiobox radioButton)
        {
            radioButton.Click += RadioButton_Click;
            _radioButtons.Add(radioButton);
        }

        private void RadioButton_Click(object sender, EventArgs e)
        {
            foreach (var radioButton in _radioButtons.Where(x=>!x.Equals(sender)))
            {
                radioButton.IsChecked = false;
            }
        } 
    }
}
