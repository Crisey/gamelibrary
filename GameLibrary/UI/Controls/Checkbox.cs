﻿using GameLibrary.UI.Controls.Styles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Checkbox : CheckboxStyle
    {
        

        public Checkbox(Game game) : base(game)
        {
            
        }

        public void LoadContent()
        {
            CheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/Checkedbox");
            UncheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/Uncheckedbox");
            base.LoadContent();
        }
    }
}
