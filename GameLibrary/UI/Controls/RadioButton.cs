﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary.UI.Controls.Styles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Controls
{
    public class Radiobox : CheckboxStyle
    {
        public Radiobox(Game game) : base(game)
        {

        }

        public override void OnClick(object sender, EventArgs e)
        {
            if (!IsChecked)
                IsChecked = true;
        }

        public void LoadContent()
        {
            CheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/CheckedRadio");
            UncheckedTexture = Game.Content.Load<Texture2D>("Textures/UI/UncheckedRadio");
            base.LoadContent();
        }
    }
}
