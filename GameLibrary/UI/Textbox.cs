﻿using System;
using System.Collections.Generic;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameLibrary.UI
{
    public class Textbox : Control
    {
        private readonly FontsUtil _font;
        private string _renderedValue;
        private Keys _lastKey;

        private KeyboardState _oldState;
        private KeyboardState _newState;

        public Textbox(Game game) : base(game)
        {
            _font = new FontsUtil(Game);
            _renderedValue = Value;
         
            Click += OnClick;
            TextChanged += OnTextChanged;
            Confirm += (sender, args) => { Value += Environment.NewLine; };
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            _renderedValue = Value;
            var boundsWidth = Bounds.Width - Margin;
            var widthFont = (int)_font.MediumFont.MeasureString(_renderedValue).Length();

            while (widthFont > boundsWidth)
            {
                _renderedValue = _renderedValue.Substring(1);

                widthFont = (int)_font.MediumFont.MeasureString(_renderedValue).Length();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            Vector2 pos = new Vector2(Bounds.X - Margin + Bounds.Width - _font.MediumFont.MeasureString(_renderedValue).X, y: Bounds.Y + Bounds.Height / 2 - (_font.MediumFont.MeasureString(_renderedValue).Y / 2) + 1);
            spriteBatch.Begin();
            spriteBatch.Draw(Texture, Bounds, Color.White);
            spriteBatch.DrawString(_font.MediumFont, _renderedValue, pos, Color.White);
            spriteBatch.End();
        }

        public override void LoadContent()
        {
            base.LoadContent();
            Texture = Game.Content.Load<Texture2D>("Textures/UI/Textbox");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (!IsSelected) return;
            _newState = Keyboard.GetState();
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                if (_newState.IsKeyDown(key) && _oldState.IsKeyUp(key))
                {
                    if (key == Keys.Enter) { OnConfirm(EventArgs.Empty); }

                    else if (key == Keys.Space) Value += " ";
                    else if (key == Keys.OemTilde && IsSpecialChar()) Value += "~";
                    else if (key == Keys.OemTilde) Value += "`";
                    else if (key == Keys.D1 && IsSpecialChar()) Value += "!";
                    else if (key == Keys.D2 && IsSpecialChar()) Value += "@";
                    else if (key == Keys.D3 && IsSpecialChar()) Value += "#";
                    else if (key == Keys.D4 && IsSpecialChar()) Value += "$";
                    else if (key == Keys.D5 && IsSpecialChar()) Value += "%";
                    else if (key == Keys.D6 && IsSpecialChar()) Value += "^";
                    else if (key == Keys.D7 && IsSpecialChar()) Value += "&";
                    else if (key == Keys.D8 && IsSpecialChar()) Value += "*";
                    else if (key == Keys.D9 && IsSpecialChar()) Value += "(";
                    else if (key == Keys.D0 && IsSpecialChar()) Value += ")";
                    else if (key == Keys.OemPipe && IsSpecialChar()) Value += "|";
                    else if (key == Keys.OemPipe) Value += @"\";
                    else if (key == Keys.OemOpenBrackets && IsSpecialChar()) Value += "{";
                    else if (key == Keys.OemCloseBrackets && IsSpecialChar()) Value += "}";
                    else if (key == Keys.OemOpenBrackets) Value += "[";
                    else if (key == Keys.OemCloseBrackets) Value += "]";
                    else if (key == Keys.Tab) Value += "    ";
                    else if (key >= Keys.D0 && key <= Keys.D9)
                    {
                        Value += key.ToString().Substring(1);
                    }
                    else if (key.ToString().Length > 1) ;
                    else
                    {
                        Value += key.ToString();
                    }
                }
                else if (_newState.IsKeyDown(key))
                {
                    if (key == Keys.Back)
                    {
                        if (!string.IsNullOrEmpty(Value))
                        {
                            Value = Value.Remove(Value.Length - 1);
                        }
                    }
                }
            }
            _oldState = _newState;
        }

        private bool IsSpecialChar()
        {
            return (_newState.IsKeyDown(Keys.LeftShift) || _newState.IsKeyDown(Keys.RightShift));
        }

        private void OnClick(object sender, EventArgs e)
        {
            IsSelected = IsInArea();
        }

        public int Margin { get; set; } = 5;
        public bool IsSelected { get; set; }

        private string _value;
        public override string Value
        {
            get
            {
                if (_value == null)
                    return string.Empty;
                return _value;
            }
            set
            {
                _value = value;
                OnTextChanged(EventArgs.Empty);
            }
        }

        public event EventHandler TextChanged;
        protected virtual void OnTextChanged(EventArgs e)
        {
            EventHandler eh = TextChanged;
            if (eh != null)
            {
                eh(this, e);
            }
        }

        public event EventHandler Confirm;
        protected virtual void OnConfirm(EventArgs e)
        {
            EventHandler eh = Confirm;
            if (eh != null)
            {
                eh(this, e);
            }
        }

        protected override void OnMouseClick(object sender, EventArgs e)
        {
            OnClick(EventArgs.Empty);
        }
    }
}
