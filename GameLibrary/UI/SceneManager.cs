﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameLibrary.UI
{
    public class SceneManager : GameComponent
    {
        public List<Scene> Scenes { get; set; }
        public Scene ActualScene { get; private set; }

        public SceneManager(Game game) : base(game)
        {
            Scenes = new List<Scene>();
        }

        public void SetActualScene(string nameScene)
        {
            ActualScene = Scenes.Find(x => x.Name.Equals(nameScene));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }

    public class Scene
    {
        public string Name { get; set; }


        public Scene()
        {
            
        }

        public Scene(string name)
        {
            Name = name;
        }
    }
}
