﻿using System;
using GameLibrary.UI.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI
{
    public class Button : Control
    {
        private FontsUtil _font;

        public Button(Game game) : base(game)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            spriteBatch.Begin();
            var position = new Vector2(x: Bounds.Width / 2 - _font.MediumFont.MeasureString(Value).X / 2 + Bounds.X,
                                       y: Bounds.Y + Bounds.Height / 2 - (_font.MediumFont.MeasureString(Value).Y / 2) + 1);
            spriteBatch.DrawString(_font.MediumFont, Value, position, Color.White);
            spriteBatch.End();

        }

        public void LoadContent()
        {
            base.LoadContent();
            _font = new FontsUtil(Game);
            Texture = Game.Content.Load<Texture2D>("Textures/UI/Button");
            AutoPosition = true;
        }

        #region Properties
        private string _value = "Button";
        private bool _autoPosition;

        public bool AutoPosition
        {
            get { return _autoPosition; }
            set
            {
                if (value)
                {
                    Bounds = new Rectangle(Bounds.X, Bounds.Y, (int)_font.MediumFont.MeasureString(Value).X + 20, (int)_font.MediumFont.MeasureString(Value).Y);
                    Console.WriteLine(Bounds);
                }
                _autoPosition = value;
            }
        }

        public override string Value
        {
            get { return _value; }
            set
            {
                this._value = value;
                AutoPosition = true;
            }
        }

        #endregion Properties

        #region Events

        #endregion Events
    }
}
