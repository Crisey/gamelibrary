﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Core
{
    public class FontsUtil : GameComponent
    {
        private SpriteFont _smallFont;
        private SpriteFont _mediumFont;
        private SpriteFont _largeFont;

        private string SmallFontPath { get;} = "Fonts/UtilsFont";
        private string MediumFontPath { get;} = "Fonts/UtilsFont";
        private string LargeFontPath { get;} = "Fonts/UtilsFont";

        public SpriteFont LargeFont
        {
            get { return _largeFont ?? (_largeFont = Game.Content.Load<SpriteFont>(LargeFontPath)); }
            private set { _largeFont = value; }
        }

        public SpriteFont SmallFont
        {
            get { return _smallFont ?? (_smallFont = Game.Content.Load<SpriteFont>(SmallFontPath)); }
            private set { _smallFont = value; }
        }

        public SpriteFont MediumFont
        {
            get { return _mediumFont ?? (_mediumFont = Game.Content.Load<SpriteFont>(MediumFontPath)); }
            private set { _mediumFont = value; }
        }

        public FontsUtil(Game game) : base(game)
        {

        }
    }
}
