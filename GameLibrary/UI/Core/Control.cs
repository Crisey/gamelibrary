﻿using System;
using GameLibrary.Mouse;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary.UI.Core
{
    public abstract class Control : DrawableGameComponent
    {
        public Text Text { get; set; }
        public virtual Rectangle Bounds
        {
            get
            {
                return _bounds;
            }
            set
            {
                _bounds = value;
                OnBoundsChanged(EventArgs.Empty);
            }
        }
        protected FontsUtil Font;
        protected Texture2D Texture
        {
            get { return _texture ?? (_texture = Game.Content.Load<Texture2D>("Textures/Default")); }
            set { _texture = value; }
        }
        protected Rectangle _bounds;
        private Texture2D _texture;
        private readonly MouseInput _mouseInput;

        protected Control(Game game) : base(game)
        {
            _mouseInput = new MouseInput(game);
            Text = new Text();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            Font = new FontsUtil(Game);

            _mouseInput.MouseLeftClick += OnMouseClick;
            _mouseInput.MouseRightClick += OnMouseClick;
            _mouseInput.MouseDown += (sender, args) => { if (IsInArea()) OnMouseDown(EventArgs.Empty); };
            _mouseInput.MouseUp += (sender, args) => { if (IsInArea()) OnMouseUp(EventArgs.Empty); };
        }
        public virtual new void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            _mouseInput.Update(gameTime);

            if (IsInArea())
                OnMouseHover(EventArgs.Empty);
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(blendState: BlendState.AlphaBlend, sortMode: SpriteSortMode.Immediate);
            spriteBatch.Draw(Texture, Bounds, Color.White);
            spriteBatch.End();
        }
        protected bool IsInArea()
        {
            return _mouseInput.MousePosition.X > Bounds.X &&
                   _mouseInput.MousePosition.X < Bounds.X + Bounds.Width &&
                   _mouseInput.MousePosition.Y > Bounds.Y &&
                   _mouseInput.MousePosition.Y < Bounds.Y + Bounds.Height;
        }
        protected virtual void OnMouseClick(object sender, EventArgs e)
        {
            if (IsInArea())
                OnClick(EventArgs.Empty);
            OnClickElseWhere(EventArgs.Empty);
        }

        internal event EventHandler Click;
        public event EventHandler MouseHover;
        public event EventHandler MouseDown;
        public event EventHandler MouseUp;
        public event EventHandler BoundsChanged;
        public event EventHandler ClickElseWhere;

        private void OnMouseDown(EventArgs e)
        {
            EventHandler eh = MouseDown;
            eh?.Invoke(this, e);
        }
        private void OnMouseUp(EventArgs e)
        {
            EventHandler eh = MouseUp;
            eh?.Invoke(this, e);
        }
        private void OnMouseHover(EventArgs e)
        {
            EventHandler eh = MouseHover;
            eh?.Invoke(this, e);
        }
        protected virtual void OnClick(EventArgs e)
        {
            EventHandler eh = Click;
            eh?.Invoke(this, e);
        }
        protected void OnBoundsChanged(EventArgs e)
        {
            EventHandler eh = BoundsChanged;
            eh?.Invoke(this, e);
        }
        private void OnClickElseWhere(EventArgs e)
        {
            EventHandler eh =  ClickElseWhere;
            if (eh != null)
            {
                eh(this, e);
            }
        }
    }
}
