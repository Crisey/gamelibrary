﻿using System;

namespace GameLibrary.Utils
{
    public class ThreadSafeRandom
    {
        private static readonly Random _global = new Random();
        [ThreadStatic]
        private static Random _local;

        public ThreadSafeRandom()
        {
            if (_local == null)
            {
                int seed;
                lock (_global)
                {
                    seed = _global.Next();
                }
                _local = new Random(seed);
            }
        }
        public int Next()
        {
            return _local.Next();
        }
        public int Next(int max)
        {
            return _local.Next(max);
        }
        public int Next(int min, int max)
        {
            return _local.Next(min, max);
        }
        public double NextDouble()
        {
            return _local.NextDouble();
        }

        public double NextDouble(int max)
        {
            return (double)_local.Next(max * 100) / 100;
        }
        public double NextDouble(int min, int max)
        {
            return (double)_local.Next(min * 100, max *100) / 100;
        }
    }
}
